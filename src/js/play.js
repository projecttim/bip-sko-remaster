import { Player } from "./player.js";
import { Board } from "./board.js";
import { Deck } from "./deck.js";

function firstDraw() {
    let freshCards = []
    for (let i = 0; i < 8; i++) {
        freshCards.push(newDeck.drawCard());
    }
    return freshCards;
}

let newDeck = new Deck([5, 8, 8]);
let newPlayer = new Player("Robert-Jan", firstDraw());
let newBoard = new Board(newDeck.drawCard(), 5);

const allCards = document.querySelectorAll('.handcard');
allCards.forEach((element, elKey) => {
    element.addEventListener('click', () => newPlayer.activateCard(elKey, false, false), false);
});

const allBoardCards = document.querySelectorAll('.boardcard');
allBoardCards.forEach((element, key) => {
    element.addEventListener('click', () => newPlayer.playCard(key), false);
});

const allAwayCards = document.querySelectorAll('.putawaycard');
allAwayCards.forEach((element, key) => {
    element.addEventListener('click', () => newPlayer.activateOrStoreSmall(key, true), false);
});

const setCard = document.getElementById('currentSetCard99');
setCard.addEventListener('click', () => newPlayer.activateCard(99, true), false);

export { newDeck };
