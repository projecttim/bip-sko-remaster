import { Base } from "./base.js";
import { newDeck } from "./play.js";
export { Board };

class Board extends Base {
    constructor(card, specialAmount) {
        super();
        this.addToBoard("currentSetCard", 99, card);
        this.specialAmount = specialAmount;
        document.querySelector('#amountLeft').innerHTML = this.specialAmount;

    }

    refreshBoardState() {
        const allBoardCards = document.querySelectorAll('.boardcard');
        let boardCardArray = [];

        allBoardCards.forEach(boardcard => {
            if (boardcard.innerHTML != "") {
                boardCardArray.push(Number(boardcard.querySelector('div h1').innerHTML));
            } else {
                boardCardArray.push(0);
            }
        });

        const allAwayCards = document.querySelectorAll('.awaycard');
        let awayCardArray = [];
        allAwayCards.forEach(awaycard => {
            if (awaycard.innerHTML != "") {
                if (awaycard.querySelector('div h1').innerHTML == "W") {
                    awayCardArray.push("W");
                } else {
                    awayCardArray.push(Number(awaycard.querySelector('div h1').innerHTML));
                }
            } else {
                awayCardArray.push(0);
            }
        });

        const allHandCards = document.querySelectorAll('.handcard');
        let handCardArray = [];
        allHandCards.forEach(handcard => {
            if (handcard.innerHTML != "") {
                if (handcard.querySelector('div h1').innerHTML == "W") {
                    handCardArray.push("W");
                } else {
                    handCardArray.push(Number(handcard.querySelector('div h1').innerHTML));
                }
            } else {
                handCardArray.push(0);
            }
        });

        const setCardSlot = document.querySelector('.setcard');
        let setCard;
        if (setCardSlot.innerHTML != "") {
            setCard = Number(setCardSlot.querySelector('div h1').innerHTML);
        }

        return [boardCardArray, handCardArray, awayCardArray, setCard];
    }

    verifyPlay(current, newCard, slotId) {
        switch (newCard) {
            case 'W':
                document.querySelector(`#board${slotId}`).innerHTML = "";
                return true;
            case Number(current) + 1:
                document.querySelector(`#board${slotId}`).innerHTML = "";
                return true;
            default:
                return false;
        }
    }

    nextSpecial() {
        document.querySelector(`#currentSetCard99`).innerHTML = "";
        this.addToBoard("currentSetCard", 99, newDeck.drawCard());
        let newSpecialAmount = document.querySelector('#amountLeft');
        newSpecialAmount.innerHTML = Number(newSpecialAmount.innerHTML) - 1;
        if (newSpecialAmount.innerHTML == 0) {  
            //win game
            console.log('gewonnen lozertj');
        }
    }
}

