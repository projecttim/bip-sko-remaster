import { Base } from "./base.js";
export { Deck };

class Deck extends Base {
    constructor(cardAmountArray) {
        super();
        this.cards = [];
        for (let x = 0; x < 4; x++) {
            for (let i = 1; i <= 13; i++) {
                if (i == 13) {
                    this.cards.push("W");
                } else {
                    this.cards.push(i);
                }
            }
        }
        let order = ['board', 'putaway', 'hand'];
        document.querySelector('#amountLeft').innerHTML = this.specialAmount;
        order.forEach((type, key) => {
            
            const storeContainer = document.getElementById(`${type}Div`);
            for (let i = 0; i < cardAmountArray[key]; i++) {
                let template = document.getElementById(`${type}Template`);
                let clone = document.importNode(template.content, true);
                storeContainer.append(clone);
             }
            let needsId = document.querySelectorAll(`.${type}card`);
            needsId.forEach((node, id) => {
                node.setAttribute('id', `${type}${id}`);
            });
        });


    }

    drawCard() {
        let ranKey = Math.floor(Math.random() * this.cards.length);
        for(let i = 0; i < this.cards.length; i++){ 
            if (i === ranKey) { 
                return this.cards.splice(i, 1)[0]; 
            }
        }
    }
}