export { Base };

class Base {
    constructor() {

    }

    getSlotValue(locId) {
        const exists = document.querySelector(`#board${locId} div h1`);
        if (exists !== null) {
            console.log(Number(exists.innerHTML));
            return Number(exists.innerHTML);
        } else {
            return 0;
        }
    }

    addToBoard(loc, locId, cardVal, currentVal) {
        let clone;
        if (loc == "putaway") {
            let template = document.getElementById('smallCard');
            clone = document.importNode(template.content, true);
        } else {
            if (cardVal == "W" && loc == "board") {
                cardVal = Number(currentVal) + 1;
            }
            let template = document.getElementById('cardTemplate');
            clone = document.importNode(template.content, true);
        }

        clone.querySelector('p').innerHTML = cardVal;
        clone.querySelector('h1').innerHTML = cardVal;
        
        const cardSlot = document.getElementById(`${loc}${locId}`);

        cardSlot.innerHTML = "";
        cardSlot.classList.remove("border-dashed", "border-4", "border-slate-400");
        
        cardSlot.append(clone);
        cardSlot.querySelector('div').classList.add("border-4", "border-blue-400");
    }

    removeFromBoard(loc, locId) {
        const cardSlot = document.getElementById(`${loc}${locId}`);
        cardSlot.innerHTML = "";
        cardSlot.classList.add("border-dashed", "border-4", "border-slate-400");
    }
}