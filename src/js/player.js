import { Base } from "./base.js";
import { Board } from "./board.js";
export { Player };

class Player extends Base {
    constructor(name, cards) {
        super();
        this.playerName = name;
        this.hand = [];
        this.board = new Board();
        
        for (let i = 0; i < cards.length; i++) {
            this.hand.push(cards[i]);
            this.addToBoard("hand", i, cards[i]);
        }
    }

    unselectAll() {
        const currentSetCard = document.querySelector(`#currentSetCard99 div`)
        currentSetCard.classList.remove("border-2", "border-blue-600", "shadow-xl", "-translate-y-4");

        const allCards = document.querySelectorAll('.handcard');
        allCards.forEach(curNode => {
            if (curNode.innerHTML != "") {
                curNode.querySelector("div").classList.remove("border-2", "border-blue-600", "shadow-xl", "-translate-y-4");
            }
        });

        const awayCard = document.querySelectorAll('.putawaycard');
        awayCard.forEach(curNode => {
            if (curNode.innerHTML != "") {
                curNode.querySelector("div").classList.remove("border-2", "border-blue-600", "shadow-xl", "-translate-y-4");
            }
        });
        
        return currentSetCard;
    }

    activateCard(id, setCard, small) {
        let currentSetCard = this.unselectAll()

        if (setCard) {
            currentSetCard.classList.add("border-blue-600", "shadow-xl", "-translate-y-4");
        } else {
            if (document.getElementById(`hand${id}`).innerHTML != "") {
                const selected = document.getElementById(`hand${id}`);
                selected.querySelector("div").classList.add("border-blue-600", "shadow-xl", "-translate-y-4");
            }
        }
        this.activeCard = id;
        this.smallCard = false;
    }

    playCard(boardPos) {
        let activeCardValue;
        if (this.activeCard == 99) {
            activeCardValue = document.querySelector(`#currentSetCard99 div h1`).innerHTML;
        } else {
            console.log(this.activeCard);
            if (this.smallCard) {
                if (document.querySelector(`#putaway${this.activeCard}`).innerHTML != "") {
                    activeCardValue = document.querySelector(`#putaway${this.activeCard} div h1`).innerHTML;
                }
            } else {
                if (document.querySelector(`#hand${this.activeCard}`).innerHTML != "") {
                    activeCardValue = document.querySelector(`#hand${this.activeCard} div h1`).innerHTML;
                }
            }
        }
        let exists = document.querySelector(`#board${boardPos} div h1`);
        let boardCardValue;
        if (exists !== null) {
            boardCardValue = document.querySelector(`#board${boardPos} div h1`).innerHTML;
        } else {
            boardCardValue = 0;
        }

        if (activeCardValue != "W") {
            activeCardValue = Number(activeCardValue);
        }

        let currentVal = this.getSlotValue(boardPos);

        if (this.board.verifyPlay(currentVal, activeCardValue, boardPos)) {
            if (this.activeCard == 99) {
                this.removeFromBoard("currentSetCard", this.activeCard);
                this.board.nextSpecial()
            } else {
                if (this.smallCard) {
                    this.removeFromBoard("putaway", this.activeCard);
                } else {
                    this.removeFromBoard("hand", this.activeCard);
                }
            }
            this.addToBoard("board", boardPos, activeCardValue, currentVal);
        }
        console.table(this.board.refreshBoardState());
    }

    activateOrStoreSmall(smallId) {
        this.unselectAll();
        const smallSlot = document.getElementById(`putaway${smallId}`);
        if (smallSlot.innerHTML != "") {
            this.activeCard = smallId;
            this.smallCard = true;
            smallSlot.querySelector("div").classList.add("border-blue-600", "shadow-xl", "-translate-y-4");
        } else {
            let activeCardSlot = document.querySelector(`#hand${this.activeCard}`);
            if (activeCardSlot.innerHTML != "") {
                let activeCardValue = activeCardSlot.querySelector("div h1").innerHTML
                this.addToBoard('putaway', smallId, activeCardValue);
                this.removeFromBoard("hand", this.activeCard);
            }
        }
    }
}